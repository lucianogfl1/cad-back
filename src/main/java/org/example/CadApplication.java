package org.example;

import org.example.model.entity.Cliente;
import org.example.model.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class CadApplication {

    @Bean
    public CommandLineRunner run (@Autowired ClienteRepository clienteRepository){
        return args -> {
            Cliente cliente = Cliente.builder().cpf("12345678911").nome("Fulano").build();
           clienteRepository.save(cliente);
        };
    }
    public static void main(String[] args) {
        SpringApplication.run(CadApplication.class, args);

    }
}
